.PHONY: all install

all: 

install: 90-ddcutil-i2c.rules i2c-dev.conf
	$(INSTALL) -D -m 644 -t $(DESTDIR)$(prefix)/etc/udev/rules.d/  90-ddcutil-i2c.rules 
	$(INSTALL) -D -m 644 -t $(DESTDIR)$(prefix)/etc/modules-load.d/ i2c-dev.conf 
